import Head from 'next/head';
import { connect } from 'react-redux'

const Home = (props) => {
  console.log('Data dari redux store')
  console.log('Data: ', props)

  return (
    <div>
      <Head>
        <title>Create Next App</title>
      </Head>
      <div className="d-flex justify-content-center">Welcome!</div>
    </div>
  );
};

const mapStateToProps = state => {
  return {
    data: state
  }
}

export default connect(mapStateToProps)(Home);
