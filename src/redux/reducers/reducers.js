import { 
  PUT_ITEM, 
  REMOVE_ITEM,
  INCREMENT,
  DECREMENT
} from 'redux/types/index.js';

const initialState = {
  items: [],
  count: 0
};

const putItem = (state, action) => Object.assign({}, state, {
  items: state.items.push(action.item)
});

const removeItem = (state) => Object.assign({}, state, {
  items: state.items.pop()
});

const increment = (state) => Object.assign({}, state, {
  count: state.count++
})

const decrement = (state) => Object.assign({}, state, {
  count: state.count--
})

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case PUT_ITEM:
      return putItem(state, action);
    case REMOVE_ITEM:
      return removeItem(state);
    case INCREMENT:
      return increment(state);
    case DECREMENT:
      return decrement(state)
    default:
      return state;
  }
};

export default reducer
