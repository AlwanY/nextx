import { 
  PUT_ITEM, 
  REMOVE_ITEM,
  INCREMENT,
  DECREMENT
} from 'redux/types/index.js';

export const putItem = (item) => ({
  type: PUT_ITEM,
  item,
});

export const removeItem = () => ({
  type: REMOVE_ITEM,
});

export const increment = () => ({
  type: INCREMENT
})

export const decrement = () => ({
  type: DECREMENT
})
